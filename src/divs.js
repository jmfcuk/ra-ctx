import React from 'react';
import { TheContext } from './TheContext';

const Blue = () => (
  <TheContext.Consumer>
    {ctx =>
      <div className="blue">
        <div>
          {ctx.number}
        </div>
        <div>
          {ctx.txt}
        </div>
      </div>
    }
  </TheContext.Consumer>
)

const Green = () => (
  <div className="green">
    <TheContext.Consumer>
      {
        (ctx) =>
          <div>
            <div>
              <div>{ctx.timestamp}</div>
            </div>
            <div>
              <button onClick={ctx.inc}>+</button>
              <button onClick={ctx.dec}>-</button>
              <button onClick={ctx.reset}>Reset</button>
            </div>
          </div>
      }
    </TheContext.Consumer>
    <Blue />
  </div>
)

export const Red = () => (
  <TheContext.Consumer>
    {ctx =>
      <div className="red">
        {ctx.number}
        <Green />
      </div>
    }
  </TheContext.Consumer>
)

export default Red;
