const io = require('socket.io')();

const map = new Map();

io.on('connection', (client) => {

  console.log('client connected');

  client.on('subscribeTimer', (interval) => {
    const c = client;
    const id = c.id;
    console.log(id + ' is subscribing to timer with interval ', interval);
    var i = setInterval(() => {
      c.emit('timer', new Date());
    }, interval);
    map.set(id, { handler: i, datetime: new Date() });
    dumpMap();
  });

  client.on('unsubscribeTimer', () => {
    const id = client.id;
    console.log(id + ' is unsubscribing from timer');
    var c = map.get(id);
    clearInterval(c.handler);
    map.delete(id);
    dumpMap();
  });

  io.on('disconnect', (client) => {
    const id = client.id;
    console.log(id + ' is disconnecting');
    var c = map.get(id);
    clearInterval(c.handler);
    map.delete(id);
    dumpMap();
 });
});

dumpMap = () => {

  if(map.size < 1) {
    console.log('client map is empty');
    return;
  }
  console.log('client map contains...');
  for (var [key, value] of map.entries()) {
    console.log(key + ' = ' + value);
  }
}

const port = 8000;
io.listen(port);
console.log('listening on port ', port);

