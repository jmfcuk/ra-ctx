import React, { Component } from 'react';
import { toWords } from 'number-to-words';
import { Red } from './divs';
import './App.css';
import { TheContext } from './TheContext';
import { subscribeTimer, unsubscribeTimer } from './api';

class App extends Component {

  state = {
    number: 0,
    txt: 'zero',
    timestamp: 'waiting...',
    inc: () => {
      const n = this.state.number + 1;
      this.setState({ number: n, txt: toWords(n) });
    },
    dec: () => {
      const n = this.state.number - 1;
      this.setState({ number: n, txt: toWords(n) });
    },
    reset: () => {
      const n = 0;
      this.setState({ number: n, txt: toWords(n) });
    },
  }

  subTimer = () => {
    subscribeTimer(1000, (err, ts) => {
      this.setState({ timestamp: ts });
    });
  }

  unsubTimer = () => {
    unsubscribeTimer();
  }

  render() {
    return (
      <div>
        <button onClick={this.subTimer}>Sub</button>
        <button onClick={this.unsubTimer}>Unsub</button>
        <TheContext.Provider value={this.state}><Red /></TheContext.Provider>
      </div>
    );
  }
}

export default App;
