import openSocket from 'socket.io-client';

const socket = openSocket('http://localhost:8000');
    
const onTimer = (err, timestamp, cb) => {
    if(cb)
        cb(err, timestamp);
}

export const subscribeTimer = (interval, cb) => {
    socket.on('timer', timestamp => onTimer(null, timestamp, cb));
    //socket.on('timer', timestamp => cb(null, timestamp));
    socket.emit('subscribeTimer', interval);
}

export const unsubscribeTimer = () => {
    socket.emit('unsubscribeTimer');
    socket.removeListener('timer', onTimer);
}

export default { subscribeTimer, unsubscribeTimer };

